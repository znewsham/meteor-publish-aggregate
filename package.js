Package.describe({
  name: 'znewsham:publish-aggregate',
  version: '0.0.1',
  // Brief, one-line summary of the package.
  summary: '',
  // URL to the Git repository containing the source code for this package.
  git: '',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Npm.depends({
  chai: "4.1.2",
  sinon: "6.1.5",
  "sinon-chai": "3.2.0"
});

Package.onUse(function(api) {
  api.versionsFrom('1.7');
  api.use('ecmascript');
  api.mainModule('server.js', "server");
  api.mainModule('client.js', "client");
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('aggregate');
  api.use("meteortesting:mocha");
  api.mainModule('aggregate-tests.js' ,'server');
});
