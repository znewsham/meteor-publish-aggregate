# Publishable aggregations using change streams
This package exposes the `aggregationCursor` function on a `Mongo.Collection` which allows for observing (and thus publishing) mongo aggregations. While change streams are used to notify the cursor it is out of date, the cursor re-runs with each update (throttled by default) as such, the full set of aggregations are available.

## Usage
In a classic publication:
```javascript
Meteor.publish("testAggregation", () => {
  const pipeline = [
    { $match: ... },
    { $group: { _id: "$someField", count: { $sum: 1 } } }
  ];

  //defaults shown
  const options = {
    name: `${MyCollection.name}_aggregated`,
    throttle: 10000,
    alwaysRerun: false
  };
  return MyCollection.aggregationCursor(
    pipeline,
    options
  );
});
```
Or, you could `observeChanges`
```javascript
  Meteor.methods({
    testObserve() {
      const pipeline = [
        { $match: ... },
        { $group: { _id: "$someField", count: { $sum: 1 } } }
      ];
      MyCollection.aggregationCursor(
        pipeline,
        {}
      ).observeChanges({
        added(id, doc) {
          console.log(doc);
        }
      });
    }
  });
```

## Requirements
Mongo 3.6+
Meteor 1.7+

## Performance
One change stream is initialised per collection the first time the `aggregationCursor` function is called, and is maintained until the last observer is stopped.

If ALL pipelines passed to `aggregationCursor` start with a `$match` the change stream will observe changes on the union of those matches only. However, it will also use the `fullDocument: updateLookup` option, which will transmit more data between server and mongo than necessary.

If any pipeline passed to `aggregationCursor` does NOT start with a `$match` the entire collection is observed.

If ALL pipelines start with a `$match` AND you don't pass in the option `alwaysRerun: true` only cursors whose results may have been changed by an update will be reran. Otherwise, all cursors are reran on all changes, however, a 10 second throttle is implemented by default. However, to detect if the cursor needs to be reran, a query is issued against the collection with tht intersection of the changed document \_ids and the `$match` of the pipeline.

## Limitations
As this package uses change streams, any aggregations which utilise `$match` will not notify you of documents which no longer match your selector, but still exist within the database. As such, limit yourself to `$match` immutable fields.

All pipelines must return documents with \_ids as their final step.

This package is a WIP - there are 100% some edge cases around timing in the multiplexer which could lead to missed notifications. Any contributions are welcome.
