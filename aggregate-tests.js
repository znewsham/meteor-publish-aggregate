import { ObserveMultiplexer, ObserveHandle, ChangeStreamWatcher, patchCursor, _publishCursor, observeChanges } from "meteor/aggregate";
import chai from "chai";
import { EJSON } from "meteor/ejson";
import sinon from "sinon";
import sinonChai from "sinon-chai";

const { expect } = chai;
chai.use(sinonChai);

/* global describe, it, beforeEach */

/* eslint-disable no-unused-expressions */

describe("publish-aggregate", () => {
  beforeEach(() => {
    ObserveMultiplexer.multiplexers = {};
    ObserveMultiplexer._id = 0;
    ChangeStreamWatcher.collectionStreams = {};
  });
  it("Calling observeChanges should return a stoppable handle", () => {
    const cursor = {
      _cursorDescription: {},
      forEach: () => {}
    };
    const handle = observeChanges.call(cursor, {});
    expect(handle).not.to.be.undefined;
    expect(handle.stop).not.to.be.undefined;
  });

  it("Calling observeChanges for the first time should create a multiplexer", () => {
    expect(Object.keys(ObserveMultiplexer.multiplexers).length).to.equal(0);
    const cursor = {
      _cursorDescription: {
        aggregate: "collection",
        pipeline: [
          { $match: {} }
        ]
      },
      forEach: () => {}
    };
    observeChanges.call(cursor, {});
    expect(Object.keys(ObserveMultiplexer.multiplexers).length).to.equal(1);
    expect(ObserveMultiplexer.multiplexers[EJSON.stringify(cursor._cursorDescription)]).not.to.be.undefined;
    expect(ObserveMultiplexer._id).to.equal(1);
  });

  it("Calling observeChanges for the second time should NOT create a multiplexer", () => {
    expect(Object.keys(ObserveMultiplexer.multiplexers).length).to.equal(0);
    const cursor = {
      _cursorDescription: {
        aggregate: "collection",
        pipeline: [
          { $match: {} }
        ]
      },
      forEach: () => {}
    };
    observeChanges.call(cursor, {});
    observeChanges.call(cursor, {});
    expect(Object.keys(ObserveMultiplexer.multiplexers).length).to.equal(1);
    expect(ObserveMultiplexer.multiplexers[EJSON.stringify(cursor._cursorDescription)]).not.to.be.undefined;
    expect(ObserveMultiplexer._id).to.equal(1);
    expect(ObserveMultiplexer.multiplexers[EJSON.stringify(cursor._cursorDescription)].handles.length).to.equal(2);
  });

  it("Calling observeChanges for the first time should create a changeStreamWatcher", () => {
    expect(Object.keys(ChangeStreamWatcher.collectionStreams).length).to.equal(0);
    const cursor = {
      _cursorDescription: {
        aggregate: "collection",
        pipeline: [
          { $match: {} }
        ]
      },
      forEach: () => {}
    };
    observeChanges.call(cursor, {});
    expect(Object.keys(ChangeStreamWatcher.collectionStreams).length).to.equal(1);
    expect(ChangeStreamWatcher.collectionStreams.collection).not.to.be.undefined;
  });

  it("Calling observeChanges for the second time on the same collection shouldnt create a changeStreamWatcher", () => {
    expect(Object.keys(ChangeStreamWatcher.collectionStreams).length).to.equal(0);
    const cursor = {
      _cursorDescription: {
        aggregate: "collection",
        pipeline: [
          { $match: {} }
        ]
      },
      forEach: () => {}
    };
    observeChanges.call(cursor, {});
    observeChanges.call(cursor, {});
    expect(Object.keys(ChangeStreamWatcher.collectionStreams).length).to.equal(1);
    expect(ChangeStreamWatcher.collectionStreams.collection).not.to.be.undefined;
  });

  it("Calling observeChanges for the second time on a different collection should create a changeStreamWatcher", () => {
    expect(Object.keys(ChangeStreamWatcher.collectionStreams).length).to.equal(0);
    const cursor = {
      _cursorDescription: {
        aggregate: "collection",
        pipeline: [
          { $match: {} }
        ]
      },
      forEach: () => {}
    };
    observeChanges.call(cursor, {});
    cursor._cursorDescription.aggregate = "collection2";
    observeChanges.call(cursor, {});
    expect(Object.keys(ChangeStreamWatcher.collectionStreams).length).to.equal(2);
    expect(ChangeStreamWatcher.collectionStreams.collection).not.to.be.undefined;
    expect(ChangeStreamWatcher.collectionStreams.collection2).not.to.be.undefined;
  });

  it("Calling stop on a handle doesnt stop a multiplexer with multiple handles", () => {
    const multiplexer = {
      handles: ["blah"],
      stop: sinon.spy()
    };
    const handle = new ObserveHandle(multiplexer, {});
    multiplexer.handles.push(handle);
    handle.stop();
    expect(multiplexer.stop).not.to.have.been.called;
  });

  it("Calling stop on a handle stops a multiplexer with no more handles", () => {
    const multiplexer = {
      handles: [],
      stop: sinon.spy()
    };
    const handle = new ObserveHandle(multiplexer, {});
    multiplexer.handles.push(handle);
    handle.stop();
    expect(multiplexer.stop).to.have.been.called;
  });

  it("Calling stop on a multiplexer doesnt call stop on a change stream watcher when it has more multiplexers", () => {
    const watcher = {
      multiplexers: ["blah"],
      stop: sinon.spy()
    };
    const multiplexer = new ObserveMultiplexer({});
    multiplexer.changeStreamWatcher = watcher;
    watcher.multiplexers.push(multiplexer);
    multiplexer.stop();
    expect(watcher.stop).not.to.have.been.called;
  });

  it("Calling stop on a multiplexer calls stop on a change stream watcher when it has no more multiplexers", () => {
    const watcher = {
      multiplexers: [],
      stop: sinon.spy()
    };
    const multiplexer = new ObserveMultiplexer({});
    multiplexer.changeStreamWatcher = watcher;
    watcher.multiplexers.push(multiplexer);
    multiplexer.stop();
    expect(watcher.stop).to.have.been.called;
  });
});
