//import { Plays } from "meteor/znewsham:justplay-common";
import { Random } from "meteor/random";
import { EJSON } from "meteor/ejson";
import { MongoID } from "meteor/mongo-id";
import { Mongo } from "meteor/mongo";
import Future from "fibers/future";

/**
  @class
  @description watch for changes on a collection, created when the first
  created the first time an aggregation is published for each collection
  destroyed the last time an aggregation publication is stopped for a collection
  always a maximum of one per connection
*/
export class ChangeStreamWatcher {
  constructor(collection, options) {
    this.collection = collection;
    this.clauses = {};
    this.multiplexers = [];
    this.options = options;
    this.pendingEvents = [];
    this.needsRestart = true;
  }

  /**
    @description register a new multiplexer for a specific aggregation cursor.
    If the cursor's pipeline starts with a $match, we limit the watch on the collection to the union of those matches
    If the cursor's pipeline starts with anything but a $match, we observe the entire collection
  */
  addMultiplexerAndCursor(multiplexer, cursor) {
    this.multiplexers.push(multiplexer);
    if (cursor._cursorDescription.pipeline && cursor._cursorDescription.pipeline[0] && cursor._cursorDescription.pipeline[0].$match) {
      this._addClause(multiplexer, cursor._cursorDescription.pipeline[0].$match);
    }
    else {
      this.watchEntireCollection();
    }
  }

  /**
    @private
    @description register a specific $match to a multiplexer, allows us to limit which cursors to rerun
  */
  _addClause(multiplexer, match) {
    const matchString = EJSON.stringify(match);
    const changeStreamMatch = {};
    Object.keys(match).forEach(key => {
      changeStreamMatch["fullDocument." + key] = match[key];
    });
    if (!this.clauses[matchString]) {
      this.clauses[matchString] = {
        changeStreamMatch,
        match,
        multiplexers: [multiplexer]
      };
    }
    else {
      this.clauses[matchString].multiplexers.push(multiplexer);
    }
    this.needsRestart = true;
  }

  /**
    @description we need to watch the entire collection
    (e.g., because we published an aggregation that didnt start with $match
  */
  watchEntireCollection() {
    this.clauses = {};
  }

  /**
    @private
    @description called by the change stream whenever a change is made (throttled)
    Either reruns all multiplexers (e.g., if we're observing the entire collection, or we're forcing)
    Or for each clause, checks if at least one document in the set of updated documents matches that multiplexer's query, and rerun just those
  */
  _handleChangeEvents() {
    const documentIds = this.pendingEvents.map(e => e.documentKey._id);
    console.log(this.pendingEvents);
    if (this.options.alwaysRerun || this.clauses.length === 0) {
      this.multiplexers.forEach(h => h.run());
    }
    else {
      Object.keys(this.clauses).forEach(key => {
        const future = new Future();
        const res = this.collection.find({
          $and: [
            { _id: { $in: documentIds } },
            this.clauses[key].match
          ]
        })
        .count()
        .then(count => future.return(count))
        .catch(err => future.throw(err));
        const count = future.wait();
        if (count) {
          console.log("update", key);
          this.clauses[key].multiplexers.forEach(h => h.run());
        }
      });
    }
    this.pendingEvents.splice(0, this.pendingEvents.length);
  }

  /**
    @description (re)start the change stream with the new arguments, Called each time a cursor is published
  */
  start() {
    if (!this.needsRestart) {
      return;
    }
    if (this.stream) {
      const future = new Future();
      if (this.stream.cursor) {
        this.stream.cursor.removeAllListeners("error");
      }
      this.stream.removeAllListeners("error");
      this.stream.removeAllListeners("data");
      this.stream.removeAllListeners("change");
      this.stream.removeAllListeners("end");
      this.stream.removeAllListeners("close");
      this.stream.on("error", console.log);
      this.stream.close(() => future.return());
      future.wait();
      console.log(this.stream.isClosed());
    }
    if (Object.keys(this.clauses).length === 0) {
      this.stream = this.collection.watch([]);
    }
    else {
      console.log("clauses", Object.values(this.clauses).map(c => c.changeStreamMatch).length);
      this.stream = this.collection.watch(
        [
          { $match: { $or: Object.values(this.clauses).map(c => c.changeStreamMatch) } }
        ],
        {
          fullDocument: "updateLookup"
        }
      );
    }
    this.needsRestart = false;
    const handleChangeEvents = this.options.throttle ? _.throttle(this._handleChangeEvents.bind(this), this.options.throttle) : this._handleChangeEvents;
    this.stream.on("change", Meteor.bindEnvironment((event) => {
      this.pendingEvents.push(event);
      handleChangeEvents.call(this);
    }));
    this.stream.on("error", Meteor.bindEnvironment((error) => {
      console.log(error);
    }));
  }

  /**
    @description called when the last multiplexer using this query is closed
  */
  stop() {
    this.stream.close();
  }
}
//track the existing change streams
ChangeStreamWatcher.collectionStreams = {};

// multiton pattern, create exactly one ChangeStreamWatcher per collection
ChangeStreamWatcher.watcherForCollection = function watcherForCollection(collection, collectionName) {
  let watcher;
  if (ChangeStreamWatcher.collectionStreams[collectionName]) {
    watcher = ChangeStreamWatcher.collectionStreams[collectionName];
  }
  else {
    watcher = new ChangeStreamWatcher(collection, { alwaysRerun: false });
    ChangeStreamWatcher.collectionStreams[collectionName] = watcher;
    console.log("watchers", Object.keys(ChangeStreamWatcher.collectionStreams).length);
  }
  return watcher;
};

/**
  @class
  @description one multiplexer per cursor with a distinct pipeline
*/

export class ObserveMultiplexer {
  constructor(options) {
    this.handles = [];
    this.key = options.key;
    this.cursor = options.cursor;
    this._id = ++ObserveMultiplexer._id;
    this.changeStreamWatcher = null;
  }

  /**
    @description iterate over the cursor and update either exactly one handle, or all handles
    A handle is only passed in when a new observer is added.
  */
  run(aHandle) {
    const handles = aHandle ? [aHandle] : this.handles;

    // maintain a set of updated documents so we can remove existing documents
    // that no longer belong in the set
    const recordIds = {};
    const future = new Future();
    this.cursor.forEach((doc) => {
      if (!doc._id) {
        Meteor._debug("You've published an aggregation cursor with items that don't have an _id");
        return;
      }
      const recordId = MongoID.idStringify(doc._id);
      recordIds[recordId] = 1;
      handles.forEach(handle => {
        if (handle.documents[recordId]) {
          handle.callbacks.changed.call(handle, recordId, doc);
        }
        else {
          handle.callbacks.added.call(handle, recordId, doc);
          handle.documents[recordId] = 1;
        }
      });
    }, () => future.return());
    future.wait();

    // remove extraneous documents
    handles.forEach(handle => {
      Object.keys(handle.documents).forEach((recordId) => {
        if (!recordIds[recordId]) {
          handle.callbacks.removed.call(handle, recordId);
          delete handle.documents[recordId];
        }
      });
    });
  }

  /**
    @description add an observer
  */
  addHandle(handle) {
    this.handles.push(handle);
  }

  /**
    @description ran when the last observer for this multiplexers pipleine is stopped
  */
  stop() {
    delete ObserveMultiplexer.multiplexers[this.key];
    this.changeStreamWatcher.multiplexers.splice(this.changeStreamWatcher.multiplexers.indexOf(this), 1);
    Object.keys(this.changeStreamWatcher.clauses).forEach((id) => {
      const clause = this.changeStreamWatcher.clauses[id];
      if (clause.multiplexers.length === 1 && clause.multiplexers[0] === this);
      delete this.changeStreamWatcher.clauses[id];
    });
    this.changeStreamWatcher.needsRestart = true;
    if (this.changeStreamWatcher.multiplexers.length === 0) {
      this.changeStreamWatcher.stop();
    }
  }
}
ObserveMultiplexer._id = 0;
ObserveMultiplexer.multiplexers = {};

//multiton pattern, create exactly one multiplexer per collection/pipeline
ObserveMultiplexer.multiplexerForCursor = function multiplexerForCursor(cursor) {
  const multiPlexerKey = EJSON.stringify(cursor._cursorDescription);
  let multiplexer;
  if (ObserveMultiplexer.multiplexers[multiPlexerKey]) {
    multiplexer = ObserveMultiplexer.multiplexers[multiPlexerKey];
  }
  else {
    multiplexer = new ObserveMultiplexer({ key: multiPlexerKey, cursor });
    ObserveMultiplexer.multiplexers[multiPlexerKey] = multiplexer;
    multiplexer.changeStreamWatcher = ChangeStreamWatcher.watcherForCollection(cursor._collection, cursor._cursorDescription.aggregate);
    multiplexer.changeStreamWatcher.addMultiplexerAndCursor(multiplexer, cursor);
    console.log("multiplexers", Object.keys(ObserveMultiplexer.multiplexers).length);
  }
  return multiplexer;
};

/**
  @class
  @description observe the changes on a specific multiplexer
*/
export class ObserveHandle {
  constructor(multiplexer, callbacks) {
    this.documents = {};
    this._multiplexer = multiplexer;
    this.callbacks = callbacks;
  }

  /**
    @description ran when an observer is stopped
  */
  stop() {
    this._multiplexer.handles.splice(this._multiplexer.handles.indexOf(this), 1);
    if (this._multiplexer.handles.length === 0) {
      this._multiplexer.stop();
    }
  }
}

/**
  @this AggregationCursor
  @description takes an object { added: function, removed: function, changed: function }
  and registers the necessary multiplexers and change stream watchers
*/
export function observeChanges(callbacks) {
  try {
    const multiplexer = ObserveMultiplexer.multiplexerForCursor(this);
    const handle = new ObserveHandle(multiplexer, callbacks);
    multiplexer.addHandle(handle);
    if (!callbacks.__suppress_initial) {
      multiplexer.run(handle);
    }
    multiplexer.changeStreamWatcher.start();
    return handle;
  }
  catch (e) {
    console.log(e);
    throw e;
  }
}

/**
  @this AggregationCursor
  @description called by meteors default publish method
*/
export function _publishCursor(sub) {
  const self = this;
  try {
  const observerHandle = this.observeChanges({
    added(id, fields) {
      sub.added(self._name, id, fields);
    },
    changed(id, fields) {
      sub.changed(self._name, id, fields);
    },
    removed(id) {
      sub.removed(self._name, id);
    }
  });
  sub.onStop(() => {
    observerHandle.stop()
  });
  return observerHandle;
}
catch(e) {
  console.log(e);
  throw e;
}
}

/**
  @description patch a cursor to ensure it has a name, _collectoin and the observer methods
*/
export function patchCursor(cursor, options) {
  cursor._collection = options.collection.rawCollection();
  cursor.observeChanges = observeChanges;
  cursor._publishCursor = _publishCursor;
  cursor._name = options.name;
}

/**
  @this Collection
  @description return a publishable aggregation cursor
  NOTE: would like to wrap aggregate but want to avoid a collission with other packages that abstract the cursor.
*/
Mongo.Collection.prototype.aggregationCursor = function aggregationCursor(pipeline, options = {}) {
  try {
    const cursor = this.rawCollection().aggregate(pipeline);
    cursor._cursorDescription = cursor.cmd;
    options.collection = this;
    if (options.throttle === undefined) {
      options.throttle = 10000;
    }
    if (!options.name) {
      options.name = `${cursor.cmd.aggregate}_aggregated`;
    }
    patchCursor(cursor, options);
    return cursor;
  }
  catch(e) {
    console.log(e);
    throw e;
  }
};
